Description: Remove pre-built manpages, so they are re-built from source
Author: Ximin Luo <infinity0@debian.org>
Origin: vendor
Forwarded: not-needed
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/doc/flashproxy-client.1
+++ /dev/null
@@ -1,154 +0,0 @@
-'\" t
-.\"     Title: flashproxy-client
-.\"    Author: [FIXME: author] [see http://docbook.sf.net/el/author]
-.\" Generator: DocBook XSL Stylesheets v1.78.1 <http://docbook.sf.net/>
-.\"      Date: 05/07/2014
-.\"    Manual: \ \&
-.\"    Source: \ \&
-.\"  Language: English
-.\"
-.TH "FLASHPROXY\-CLIENT" "1" "05/07/2014" "\ \&" "\ \&"
-.\" -----------------------------------------------------------------
-.\" * Define some portability stuff
-.\" -----------------------------------------------------------------
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.\" http://bugs.debian.org/507673
-.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.ie \n(.g .ds Aq \(aq
-.el       .ds Aq '
-.\" -----------------------------------------------------------------
-.\" * set default formatting
-.\" -----------------------------------------------------------------
-.\" disable hyphenation
-.nh
-.\" disable justification (adjust text to left margin only)
-.ad l
-.\" -----------------------------------------------------------------
-.\" * MAIN CONTENT STARTS HERE *
-.\" -----------------------------------------------------------------
-.SH "NAME"
-flashproxy-client \- The flash proxy client transport plugin
-.SH "SYNOPSIS"
-.sp
-\fBflashproxy\-client\fR \fB\-\-register\fR [\fIOPTIONS\fR] [\fILOCAL\fR][:\fIPORT\fR] [\fIREMOTE\fR][:\fIPORT\fR]
-.SH "DESCRIPTION"
-.sp
-Wait for connections on a local and a remote port\&. When any pair of connections exists, data is ferried between them until one side is closed\&. By default \fILOCAL\fR is localhost addresses on port 9001 and \fIREMOTE\fR is all addresses on port 9000\&.
-.sp
-The local connection acts as a SOCKS4a proxy, but the host and port in the SOCKS request are ignored and the local connection is always linked to a remote connection\&.
-.sp
-By default, runs as a managed proxy: informs a parent Tor process of support for the "flashproxy" or "websocket" pluggable transport\&. In managed mode, the \fILOCAL\fR port is chosen arbitrarily instead of defaulting to 9001; however this can be overridden by including a \fILOCAL\fR port in the command\&. This is the way the program should be invoked in a torrc ClientTransportPlugin "exec" line\&. Use the \fB\-\-external\fR option to run as an external proxy that does not interact with Tor\&.
-.sp
-If any of the \fB\-\-register\fR, \fB\-\-register\-addr\fR, or \fB\-\-register\-methods\fR options are used, then your IP address will be sent to the facilitator so that proxies can connect to you\&. You need to register in some way in order to get any service\&. The \fB\-\-facilitator\fR option allows controlling which facilitator is used; if omitted, it uses a public default\&.
-.SH "OPTIONS"
-.PP
-\fB\-4\fR
-.RS 4
-Registration helpers use IPv4\&.
-.RE
-.PP
-\fB\-6\fR
-.RS 4
-Registration helpers use IPv6\&.
-.RE
-.PP
-\fB\-\-daemon\fR
-.RS 4
-Daemonize (Unix only)\&.
-.RE
-.PP
-\fB\-\-external\fR
-.RS 4
-Be an external proxy (don\(cqt interact with Tor using environment variables and stdout)\&.
-.RE
-.PP
-\fB\-f\fR, \fB\-\-facilitator\fR=\fIURL\fR
-.RS 4
-Advertise willingness to receive connections to URL\&.
-.RE
-.PP
-\fB\-\-facilitator\-pubkey\fR=\fIFILENAME\fR
-.RS 4
-Encrypt registrations to the given PEM\-formatted public key (default built\-in)\&.
-.RE
-.PP
-\fB\-h\fR, \fB\-\-help\fR
-.RS 4
-Display a help message and exit\&.
-.RE
-.PP
-\fB\-l\fR, \fB\-\-log\fR=\fIFILENAME\fR
-.RS 4
-Write log to
-\fIFILENAME\fR
-(default is stdout)\&.
-.RE
-.PP
-\fB\-\-pidfile\fR=\fIFILENAME\fR
-.RS 4
-Write PID to
-\fIFILENAME\fR
-after daemonizing\&.
-.RE
-.PP
-\fB\-\-port\-forwarding\fR
-.RS 4
-Attempt to forward
-\fIREMOTE\fR
-port\&.
-.RE
-.PP
-\fB\-\-port\-forwarding\-helper\fR=\fIPROGRAM\fR
-.RS 4
-Use the given
-\fIPROGRAM\fR
-to forward ports (default "tor\-fw\-helper")\&. Implies
-\fB\-\-port\-forwarding\fR\&.
-.RE
-.PP
-\fB\-\-port\-forwarding\-external\fR=\fIPORT\fR
-.RS 4
-Forward the external
-\fIPORT\fR
-to
-\fIREMOTE\fR
-on the local host (default same as REMOTE)\&. Implies
-\fB\-\-port\-forwarding\fR\&.
-.RE
-.PP
-\fB\-r\fR, \fB\-\-register\fR
-.RS 4
-Register with the facilitator\&.
-.RE
-.PP
-\fB\-\-register\-addr\fR=\fIADDR\fR
-.RS 4
-Register the given address (in case it differs from
-\fIREMOTE\fR)\&. Implies
-\fB\-\-register\fR\&.
-.RE
-.PP
-\fB\-\-register\-methods\fR=\fIMETHOD\fR[,\fIMETHOD\fR]
-.RS 4
-Register using the given comma\-separated list of methods\&. Implies
-\fB\-\-register\fR\&. Possible methods are: appspot, email, http\&. Default is "appspot,email,http"\&.
-.RE
-.PP
-\fB\-\-transport\fR=\fITRANSPORT\fR
-.RS 4
-Registrations include the fact that you intend to use the given
-\fITRANSPORT\fR
-(default "websocket")\&.
-.RE
-.PP
-\fB\-\-unsafe\-logging\fR
-.RS 4
-Don\(cqt scrub IP addresses from logs\&.
-.RE
-.SH "SEE ALSO"
-.sp
-\fBhttp://crypto\&.stanford\&.edu/flashproxy/\fR
-.SH "BUGS"
-.sp
-Please report using \fBhttps://trac\&.torproject\&.org/projects/tor\fR\&.
--- a/doc/flashproxy-reg-appspot.1
+++ /dev/null
@@ -1,85 +0,0 @@
-'\" t
-.\"     Title: flashproxy-reg-appspot
-.\"    Author: [FIXME: author] [see http://docbook.sf.net/el/author]
-.\" Generator: DocBook XSL Stylesheets v1.78.1 <http://docbook.sf.net/>
-.\"      Date: 05/07/2014
-.\"    Manual: \ \&
-.\"    Source: \ \&
-.\"  Language: English
-.\"
-.TH "FLASHPROXY\-REG\-APPSPOT" "1" "05/07/2014" "\ \&" "\ \&"
-.\" -----------------------------------------------------------------
-.\" * Define some portability stuff
-.\" -----------------------------------------------------------------
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.\" http://bugs.debian.org/507673
-.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.ie \n(.g .ds Aq \(aq
-.el       .ds Aq '
-.\" -----------------------------------------------------------------
-.\" * set default formatting
-.\" -----------------------------------------------------------------
-.\" disable hyphenation
-.nh
-.\" disable justification (adjust text to left margin only)
-.ad l
-.\" -----------------------------------------------------------------
-.\" * MAIN CONTENT STARTS HERE *
-.\" -----------------------------------------------------------------
-.SH "NAME"
-flashproxy-reg-appspot \- Register with a facilitator through Google App Engine\&.
-.SH "SYNOPSIS"
-.sp
-\fBflashproxy\-reg\-appspot\fR [\fIOPTIONS\fR] [\fIREMOTE\fR][:\fIPORT\fR]
-.SH "DESCRIPTION"
-.sp
-Register with a flash proxy facilitator through a Google App Engine app\&. By default the remote address registered is ":9000" (the external IP address is guessed)\&. It requires https://www\&.google\&.com/ not to be blocked\&.
-.sp
-This program uses a trick to talk to App Engine, even though appspot\&.com may be blocked\&. The IP address and Server Name Indication of the request are for www\&.google\&.com, but the Host header inside the request is for an appspot\&.com subdomain\&.
-.sp
-Requires the \fBflashproxy\-reg\-url\fR program\&.
-.SH "OPTIONS"
-.PP
-\fB\-4\fR
-.RS 4
-Name lookups use only IPv4\&.
-.RE
-.PP
-\fB\-6\fR
-.RS 4
-Name lookups use only IPv6\&.
-.RE
-.PP
-\fB\-\-disable\-pin\fR
-.RS 4
-Don\(cqt check the server\(cqs public key against a list of known pins\&. You can use this if the server\(cqs public key has changed and this program hasn\(cqt been updated yet\&.
-.RE
-.PP
-\fB\-\-facilitator\-pubkey\fR=\fIFILENAME\fR
-.RS 4
-Encrypt registrations to the given PEM\-formatted public key (default built\-in)\&.
-.RE
-.PP
-\fB\-h\fR, \fB\-\-help\fR
-.RS 4
-Display help message and exit\&.
-.RE
-.PP
-\fB\-\-transport\fR=\fITRANSPORT\fR
-.RS 4
-Registrations include the fact that you intend to use the given
-\fITRANSPORT\fR
-(default "websocket")\&.
-.RE
-.PP
-\fB\-\-unsafe\-logging\fR
-.RS 4
-Don\(cqt scrub IP addresses from logs\&.
-.RE
-.SH "SEE ALSO"
-.sp
-\fBhttp://crypto\&.stanford\&.edu/flashproxy/\fR
-.SH "BUGS"
-.sp
-Please report using \fBhttps://trac\&.torproject\&.org/projects/tor\fR\&.
--- a/doc/flashproxy-reg-email.1
+++ /dev/null
@@ -1,104 +0,0 @@
-'\" t
-.\"     Title: flashproxy-reg-email
-.\"    Author: [FIXME: author] [see http://docbook.sf.net/el/author]
-.\" Generator: DocBook XSL Stylesheets v1.78.1 <http://docbook.sf.net/>
-.\"      Date: 05/07/2014
-.\"    Manual: \ \&
-.\"    Source: \ \&
-.\"  Language: English
-.\"
-.TH "FLASHPROXY\-REG\-EMAIL" "1" "05/07/2014" "\ \&" "\ \&"
-.\" -----------------------------------------------------------------
-.\" * Define some portability stuff
-.\" -----------------------------------------------------------------
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.\" http://bugs.debian.org/507673
-.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.ie \n(.g .ds Aq \(aq
-.el       .ds Aq '
-.\" -----------------------------------------------------------------
-.\" * set default formatting
-.\" -----------------------------------------------------------------
-.\" disable hyphenation
-.nh
-.\" disable justification (adjust text to left margin only)
-.ad l
-.\" -----------------------------------------------------------------
-.\" * MAIN CONTENT STARTS HERE *
-.\" -----------------------------------------------------------------
-.SH "NAME"
-flashproxy-reg-email \- Register with a facilitator using the email method
-.SH "SYNOPSIS"
-.sp
-\fBflashproxy\-reg\-email\fR [\fIOPTIONS\fR] [\fIREMOTE\fR][:\fIPORT\fR]
-.SH "DESCRIPTION"
-.sp
-Register with a flash proxy facilitator through email\&. Makes a STARTTLS connection to an SMTP server and sends mail with a client IP address to a designated address\&. By default the remote address registered is ":9000" (the external IP address is guessed based on the SMTP server\(cqs response)\&.
-.sp
-Using an SMTP server or email address other than the defaults will not work unless you have made special arrangements to connect them to a facilitator\&.
-.sp
-The email address is not polled continually\&. After running the program, it may take up to a minute for the registration to be recognized\&.
-.sp
-This program requires the M2Crypto library for Python\&.
-.SH "OPTIONS"
-.PP
-\fB\-4\fR
-.RS 4
-Name lookups use only IPv4\&.
-.RE
-.PP
-\fB\-6\fR
-.RS 4
-Name lookups use only IPv6\&.
-.RE
-.PP
-\fB\-d\fR, \fB\-\-debug\fR
-.RS 4
-Enable debugging output (Python smtplib messages)\&.
-.RE
-.PP
-\fB\-\-disable\-pin\fR
-.RS 4
-Don\(cqt check the server\(cqs public key against a list of known pins\&. You can use this if the server\(cqs public key has changed and this program hasn\(cqt been updated yet\&.
-.RE
-.PP
-\fB\-e\fR, \fB\-\-email\fR=\fIADDRESS\fR
-.RS 4
-Send mail to
-\fIADDRESS\fR
-(default is "flashproxyreg\&.a@gmail\&.com")\&.
-.RE
-.PP
-\fB\-\-facilitator\-pubkey\fR=\fIFILENAME\fR
-.RS 4
-Encrypt registrations to the given PEM\-formatted public key (default built\-in)\&.
-.RE
-.PP
-\fB\-h\fR, \fB\-\-help\fR
-.RS 4
-Display help message and exit\&.
-.RE
-.PP
-\fB\-s\fR, \fB\-\-smtp\fR=\fIHOST\fR[:\fIPORT\fR]
-.RS 4
-Use the given SMTP server (default is "gmail\-smtp\-in\&.l\&.google\&.com:25")\&.
-.RE
-.PP
-\fB\-\-transport\fR=\fITRANSPORT\fR
-.RS 4
-Registrations include the fact that you intend to use the given
-\fITRANSPORT\fR
-(default "websocket")\&.
-.RE
-.PP
-\fB\-\-unsafe\-logging\fR
-.RS 4
-Don\(cqt scrub IP addresses from logs\&.
-.RE
-.SH "SEE ALSO"
-.sp
-\fBhttp://crypto\&.stanford\&.edu/flashproxy/\fR
-.SH "BUGS"
-.sp
-Please report using \fBhttps://trac\&.torproject\&.org/projects/tor\fR\&.
--- a/doc/flashproxy-reg-http.1
+++ /dev/null
@@ -1,76 +0,0 @@
-'\" t
-.\"     Title: flashproxy-reg-http
-.\"    Author: [FIXME: author] [see http://docbook.sf.net/el/author]
-.\" Generator: DocBook XSL Stylesheets v1.78.1 <http://docbook.sf.net/>
-.\"      Date: 05/07/2014
-.\"    Manual: \ \&
-.\"    Source: \ \&
-.\"  Language: English
-.\"
-.TH "FLASHPROXY\-REG\-HTTP" "1" "05/07/2014" "\ \&" "\ \&"
-.\" -----------------------------------------------------------------
-.\" * Define some portability stuff
-.\" -----------------------------------------------------------------
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.\" http://bugs.debian.org/507673
-.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.ie \n(.g .ds Aq \(aq
-.el       .ds Aq '
-.\" -----------------------------------------------------------------
-.\" * set default formatting
-.\" -----------------------------------------------------------------
-.\" disable hyphenation
-.nh
-.\" disable justification (adjust text to left margin only)
-.ad l
-.\" -----------------------------------------------------------------
-.\" * MAIN CONTENT STARTS HERE *
-.\" -----------------------------------------------------------------
-.SH "NAME"
-flashproxy-reg-http \- Register with a facilitator using the HTTP method
-.SH "SYNOPSIS"
-.sp
-\fBflashproxy\-reg\-http\fR [\fIOPTIONS\fR] [\fIREMOTE\fR][:\fIPORT\fR]
-.SH "DESCRIPTION"
-.sp
-Register with a flash proxy facilitator using an HTTP POST\&. By default the remote address registered is ":9000"\&.
-.SH "OPTIONS"
-.PP
-\fB\-4\fR
-.RS 4
-Name lookups use only IPv4\&.
-.RE
-.PP
-\fB\-6\fR
-.RS 4
-Name lookups use only IPv6\&.
-.RE
-.PP
-\fB\-f\fR, \fB\-\-facilitator\fR=\fIURL\fR
-.RS 4
-Register with the given facilitator (default "https://fp\-facilitator\&.org/")\&.
-.RE
-.PP
-\fB\-h\fR, \fB\-\-help\fR
-.RS 4
-Display help message and exit\&.
-.RE
-.PP
-\fB\-\-transport\fR=\fITRANSPORT\fR
-.RS 4
-Registrations include the fact that you intend to use the given
-\fITRANSPORT\fR
-(default "websocket")\&.
-.RE
-.PP
-\fB\-\-unsafe\-logging\fR
-.RS 4
-Don\(cqt scrub IP addresses from logs\&.
-.RE
-.SH "SEE ALSO"
-.sp
-\fBhttp://crypto\&.stanford\&.edu/flashproxy/\fR
-.SH "BUGS"
-.sp
-Please report using \fBhttps://trac\&.torproject\&.org/projects/tor\fR\&.
--- a/doc/flashproxy-reg-url.1
+++ /dev/null
@@ -1,85 +0,0 @@
-'\" t
-.\"     Title: flashproxy-reg-url
-.\"    Author: [FIXME: author] [see http://docbook.sf.net/el/author]
-.\" Generator: DocBook XSL Stylesheets v1.78.1 <http://docbook.sf.net/>
-.\"      Date: 05/07/2014
-.\"    Manual: \ \&
-.\"    Source: \ \&
-.\"  Language: English
-.\"
-.TH "FLASHPROXY\-REG\-URL" "1" "05/07/2014" "\ \&" "\ \&"
-.\" -----------------------------------------------------------------
-.\" * Define some portability stuff
-.\" -----------------------------------------------------------------
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.\" http://bugs.debian.org/507673
-.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
-.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-.ie \n(.g .ds Aq \(aq
-.el       .ds Aq '
-.\" -----------------------------------------------------------------
-.\" * set default formatting
-.\" -----------------------------------------------------------------
-.\" disable hyphenation
-.nh
-.\" disable justification (adjust text to left margin only)
-.ad l
-.\" -----------------------------------------------------------------
-.\" * MAIN CONTENT STARTS HERE *
-.\" -----------------------------------------------------------------
-.SH "NAME"
-flashproxy-reg-url \- Register with a facilitator using an indirect URL
-.SH "SYNOPSIS"
-.sp
-\fBflashproxy\-reg\-url\fR [\fIOPTIONS\fR] \fIREMOTE\fR[:\fIPORT\fR]
-.SH "DESCRIPTION"
-.sp
-Print a URL, which, when retrieved, will cause the client address \fIREMOTE\fR[:\fIPORT\fR] to be registered with the flash proxy facilitator\&. The default \fIPORT\fR is 9000\&.
-.SH "OPTIONS"
-.PP
-\fB\-f\fR, \fB\-\-facilitator\fR=\fIURL\fR
-.RS 4
-Register with the given facilitator (default "https://fp\-facilitator\&.org/")\&.
-.RE
-.PP
-\fB\-\-facilitator\-pubkey\fR=\fIFILENAME\fR
-.RS 4
-Encrypt registrations to the given PEM\-formatted public key (default built\-in)\&.
-.RE
-.PP
-\fB\-h\fR, \fB\-\-help\fR
-.RS 4
-Display help message and exit\&.
-.RE
-.PP
-\fB\-\-transport\fR=\fITRANSPORT\fR
-.RS 4
-Registrations include the fact that you intend to use the given
-\fITRANSPORT\fR
-(default "websocket")\&.
-.RE
-.SH "EXAMPLE"
-.sp
-Say you wish to register 192\&.0\&.2\&.1:9000\&. Run
-.sp
-.if n \{\
-.RS 4
-.\}
-.nf
-\&./flashproxy\-reg\-url 192\&.0\&.2\&.1:9000
-.fi
-.if n \{\
-.RE
-.\}
-.sp
-The program should output a long string looking something like
-.sp
-https://fp\-facilitator\&.org/reg/0labtDob545HeKpLZ8LqGeOi\-OK7HXoQvfQzj0P2pjh1NrCKNDaPe91zo\&.\&.\&.
-.sp
-Copy this string and paste it into any URL fetching website or program\&. Once the URL is retrieved your address will be registered with the facilitator\&.
-.SH "SEE ALSO"
-.sp
-\fBhttp://crypto\&.stanford\&.edu/flashproxy/\fR
-.SH "BUGS"
-.sp
-Please report using \fBhttps://trac\&.torproject\&.org/projects/tor\fR\&.
