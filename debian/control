Source: flashproxy
Maintainer: Debian Privacy Tools Maintainers <pkg-privacy-maintainers@lists.alioth.debian.org>
Uploaders: Ximin Luo <infinity0@debian.org>
Section: net
Priority: extra
Build-Depends: debhelper (>= 9),
 asciidoc,
 docbook-xsl,
 dh-autoreconf,
 dh-python,
 faketime,
 help2man (>= 1.47.1),
 nodejs,
 node-optimist,
 openssl,
 python,
 python-m2crypto,
 python-setuptools,
 xsltproc,
Standards-Version: 3.9.6
X-Python-Version: >= 2.7
Homepage: https://crypto.stanford.edu/flashproxy/
Vcs-Git: https://anonscm.debian.org/git/pkg-privacy/packages/flashproxy.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-privacy/packages/flashproxy.git

Package: flashproxy-common
Architecture: all
Section: python
Depends: ${misc:Depends}, ${python:Depends}
Description: Pluggable transport to circumvent IP address blocking - common library
 Flashproxy is a tool to circumvent censorship by setting up many, generally
 ephemeral, browser-based proxies, with the goal of outpacing a censor's
 ability to enumerate and block the IP addresses of those proxies. Censored
 clients can then connect to a Tor relay via these proxies, by using a
 hard-to-block, low-bandwidth registration system called the facilitator.
 .
 This package contains the common Python library used by other components.

Package: flashproxy-client
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, flashproxy-common (= ${binary:Version})
Description: Pluggable transport to circumvent IP address blocking - client transport plugin
 Flashproxy is a tool to circumvent censorship by setting up many, generally
 ephemeral, browser-based proxies, with the goal of outpacing a censor's
 ability to enumerate and block the IP addresses of those proxies. Censored
 clients can then connect to a Tor relay via these proxies, by using a
 hard-to-block, low-bandwidth registration system called the facilitator.
 .
 This package contains the client transport plugin that enables censored Tor
 users to circumvent censorship. It is compliant with the Tor pluggable
 transports specification.

Package: flashproxy-facilitator
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, flashproxy-common (= ${binary:Version}), openssl, adduser
Recommends: httpd-cgi | apache2
Suggests: flashproxy-proxy
Description: Pluggable transport to circumvent IP address blocking - facilitator
 Flashproxy is a tool to circumvent censorship by setting up many, generally
 ephemeral, browser-based proxies, with the goal of outpacing a censor's
 ability to enumerate and block the IP addresses of those proxies. Censored
 clients can then connect to a Tor relay via these proxies, by using a
 hard-to-block, low-bandwidth registration system called the facilitator.
 .
 This package contains the facilitator that accepts registrations (requests
 for service) from clients, and serves them with a browser proxy that provides
 access to a Tor relay.
 .
 Most end-users do not need to install this package.

Package: flashproxy-proxy
Architecture: all
Section: web
Depends: ${misc:Depends}
Recommends: httpd | apache2
Description: Pluggable transport to circumvent IP address blocking - browser proxy
 Flashproxy is a tool to circumvent censorship by setting up many, generally
 ephemeral, browser-based proxies, with the goal of outpacing a censor's
 ability to enumerate and block the IP addresses of those proxies. Censored
 clients can then connect to a Tor relay via these proxies, by using a
 hard-to-block, low-bandwidth registration system called the facilitator.
 .
 This package contains the browser proxy to be served from a web server.
 Visitors to this website can then act as ephemeral proxies for censored users.
 .
 Most end-users do not need to install this package.

Package: node-flashproxy
Architecture: all
Depends: ${misc:Depends}, flashproxy-proxy (= ${binary:Version}),
 nodejs, node-optimist, node-xmlhttprequest, node-ws
Description: Pluggable transport to circumvent IP address blocking - nodejs proxy
 Flashproxy is a tool to circumvent censorship by setting up many, generally
 ephemeral, browser-based proxies, with the goal of outpacing a censor's
 ability to enumerate and block the IP addresses of those proxies. Censored
 clients can then connect to a Tor relay via these proxies, by using a
 hard-to-block, low-bandwidth registration system called the facilitator.
 .
 This package contains a nodejs-based proxy to be run as a local service. It
 is for people that constantly want to act as proxies for censored users.
 Due to the design of the system, it more useful when installed on a machine
 that continually changes IP address, such as a laptop.
 .
 Most end-users do not need to install this package.
